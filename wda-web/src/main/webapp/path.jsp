<%@page import="com.farm.wda.inter.WdaAppInter"%>
<%@page import="com.farm.wda.util.AppConfig"%>
<%@page import="java.io.File"%>
<%@page import="com.farm.wda.Beanfactory"%>
<%@ page language="java" pageEncoding="utf-8"%>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><%=AppConfig.getString("config.web.title")%></title>
<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<%
	String paht = request.getParameter("path");
	String key = request.getParameter("key");
	String typename = request.getParameter("typename");
	String filename = request.getParameter("filename");
	String txt = null;
	if ("NONE".equals(typename)) {
		typename = null;
	}
	if (key == null || key.isEmpty()) {
		key = "none";
	}
	if (filename == null || filename.isEmpty()) {
		filename = key;
	}
	WdaAppInter wad = Beanfactory.getWdaAppImpl();
	if (paht != null && key != null && typename != null && !paht.isEmpty() && !key.isEmpty()
			&& !typename.isEmpty()) {
		wad.generateDoc(key, new File(paht), typename, filename);
	} else {
		if (paht != null && key != null && !paht.isEmpty() && !key.isEmpty()) {
			wad.generateDoc(key, new File(paht), filename);
		}
	}
%>
<body style="background-color: #8a8a8a;">
	<jsp:include page="/commons/head.jsp"></jsp:include>
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-body text-center">
						<%=wad.getInfo(key)%>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">文档状态</div>
					<div class="panel-body">
						<table class="table">
							<tr>
								<td>预览文件</td>
								<td>
									<%
										if (wad.isGenerated(key, "TXT")) {
									%> <a href="<%=wad.getUrl(key, "TXT")%>"><img alt="TXT"
										src="img/txt.png"></a> <%
 	}
 %> <%
 	if (wad.isGenerated(key, "HTML")) {
 %> <a href="<%=wad.getUrl(key, "HTML")%>"><img alt="HTML"
										src="img/html.png"></a> <%
 	}
 %> <%
 	if (wad.isGenerated(key, "PDF")) {
 %> <a href="<%=wad.getUrl(key, "PDF")%>"><img alt="PDF"
										src="img/pdf.png"></a> <%
 	}
 %>
								</td>
							</tr>

							<tr>
								<td>文本值</td>
								<td style="word-break:break-all;">
									<%
										try {
											txt = wad.getText(key);
										} catch (Exception e) {
											txt = e.getMessage();
										}
										if (txt.length() > 200) {
											txt = txt.substring(0, 180) + "...";
										}
									%> <%=txt%>
								</td>
							</tr>
							<tr>
								<td>日志文件</td>
								<td>
									<%
										if (wad.isLoged(key)) {
									%> <a href="<%=wad.getlogURL(key)%>"><img alt="日志文件"
										src="img/log.png"></a> <%
 	} else {
 %> <%="无日志"%> <%
 	}
 %>
								</td>
							</tr>
							<%
								if (key != null && !key.isEmpty()) {
							%>
							<tr>
								<th style="width: 100px;">文件Key</th>
								<th><%=key%></th>
							</tr>
							<%
								}
							%>
							<%
								if (paht != null && !paht.isEmpty()) {
							%>
							<tr>
								<td>文件地址</td>
								<td><%=paht%></td>
							</tr>
							<%
								}
							%>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
	<script src="js/jquery11.3.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>